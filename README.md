# LaTeX predložak za diplomski i završni rad

Neslužbeni LaTeX predložak za diplomski/završni rad Odjela za informatiku Sveučilišta u Rijeci.
Predložak sadrži i upute za manje estetske izmjene.

Autor: Edvin Močibob <[edvin.mocibob@gmail.com](mailto:edvin.mocibob@gmail.com)>